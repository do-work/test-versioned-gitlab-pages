Cluster Repository
==================

The cluster repository is a git repository. It holds all information
which define the (intended) state of a cluster. This information
consists of:

-  The version of the LCM code to deploy the cluster
-  The version of the WireGuard user information
-  State of Terraform
-  State of the WireGuard IP address management (IPAM)
-  Secrets and credentials obtained while deploying the cluster
-  defines the platform layout and other properties of the cluster

A user checks out the cluster repository and uses it to interact with
the yk8s-cluster.

Cluster Repository Structure
----------------------------

The following schema shows all non-generated files. A local checkout
will most certainly have more files than these.

::

   your_cluster_repo
   ├── config/
   │   ├── config.toml               # Cluster configuration
   │   └── wireguard_ipam.toml       # WireGuard IPAM
   ├── etc/                          # Cluster-specific files
   ├── inventory/                    # Ansible inventory
   │   └── yaook-k8s/                # Variables passed to Ansible
   │   └── hosts                     # Ansible hosts file
   ├── k8s-custom/                   # Custom Stage
   │   ├── roles/                    # Place to dump in personal Ansible roles
   │   └── main.yaml                 # Customization playbook
   ├── managed-k8s/                  # Submodule with the LCM code
   ├── submodules/                   # Place for additional git submodules
   ├── terraform/                    # Place for Terraform specific files
   │   ├── .terraform/
   │   │   └── plugins/
   │   │       └── linux_amd64/
   │   │           └── lock.json     # Terraform plugin version lock
   │   ├── terraform.tfstate         # Terraform state
   │   └── terraform.tfstate.backup  # Terraform state backup
   ├── vault/                        # Local vault data
   ├── .envrc                        # direnv (environment variables) configuration
   ├── .gitattributes
   ├── .gitignore
   └── .gitattributes

Detailed explanation:

-  ``config/config.toml`` holds the variables of
   the cluster. A template for this file can be found in the
   ``templates/`` directory.

   Note that the 
   script ``init.sh`` will bootstrap your configuration from that
   template.
