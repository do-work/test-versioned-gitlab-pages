Welcome to yaook/k8s' documentation!
====================================

main-2

.. toctree::
   :maxdepth: 2
   :caption: Concept
   :hidden:

   concepts/cluster-repository
   concepts/abstraction-layers

.. image:: img/yaook-husky.png
  :width: 250
  :alt: The Yaook-Project logo. A husky looking to the left.
  :align: center
